from numpy import pi
from geom import *
from common import *
from rules import *

def step(units):
    newstate = []
    for id, unit in enumerate(units):
        vec = strategy_step[unit['strategy']](units, unit)
        move_direction = norm(vec) * unit['speed']
        move_direction = update_move(units, id, move_direction)
        newPoint = unit['position'] + move_direction
        newUnit = unit
        newUnit['position'] = np.array(newPoint)
        newstate.append(newUnit)
    return newstate

def step_1(units, unit):
    targets = find_units(units, need_color = faster_color)
    closest_target = find_closest(targets, unit)
    res = get_vec(unit, closest_target)
    teammates = find_units(units, need_color = unit['color'])

    closer_teammates = []
    for u in teammates:
        if unit_eq(u, unit):
            continue
        if dist(unit, closest_target) < dist(closest_target, u):
            continue
        if get_angle(unit['position'], closest_target['position'], u['position']) > pi/10:
            continue
        closer_teammates.append(u)
    right_units = get_units_on_side(closer_teammates, unit, res, True)
    left_units = get_units_on_side(closer_teammates, unit, res, False)

    if len(right_units) > len(left_units):
        res = rotate(res, -pi / 2) 
    elif len(right_units) < len(left_units):
        res = rotate(res, pi / 2)

    return res

def step_2(units, unit):
    enemies = find_units(units, need_color=slower_color)
    closest = find_closest(enemies, unit)
    vec = get_vec(unit, closest)
    if dist(closest, unit) < 0.1 + closest['size'] + unit['size']:
        return rotate(vec, pi)
    return rotate(vec, pi/2)

strategy_step = {
    'test1' : step_1,
    'test2' : step_2
}
