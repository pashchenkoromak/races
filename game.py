#!/usr/bin/python3
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from step import step
from rules import *

units = []

def extract_points(color = None):
    global units
    return [x["position"][0] for x in units if x['color'] == color or color == None], [x["position"][1] for x in units if x['color'] == color or color == None]

def add_unit(pos, speed, size, color, strategy):
    global units
    units.append({'position':np.array(pos),'speed':speed,'color':color,'strategy':strategy, 'size':size})

n = 10
for i in range(n):
    add_unit((i/n * size[0], i/n * size[1]), 0.03, 0.2, slower_color, 'test1')
add_unit((2,10), 0.05, 0.2, faster_color, 'test2')
add_unit((10,2), 0.05, 0.2, faster_color, 'test2')

fig, ax = plt.subplots()

x, y = extract_points()
mat, = ax.plot(x, y, 'o', color='r')

def gen():
    i = 0
    while not game_over(units):
        i += 1
        yield i

def animate(i):
    global units
    units = step(units)
    x, y = extract_points()
    mat.set_data(x, y)
    return mat,

ax.axis([0,size[0],0,size[1]])
ani = animation.FuncAnimation(fig, animate, frames=gen, interval=1000//60, repeat = False)
plt.show()
