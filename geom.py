from numpy import cos, sin
import numpy as np
from math import sqrt

def norm(vec):
    return vec / np.linalg.norm(vec)

# currently in 2d
def rotate(vec, angle):
    cs = cos(angle)
    sn = sin(angle)    
    rx = vec[0] * cs - vec[1] * sn
    ry = vec[0] * sn + vec[1] * cs
    return np.array([rx, ry])

def get_side(p1, p2, p3) -> bool:
    return ((p2[0] - p1[0])*(p3[1] - p1[1]) - (p2[1] - p1[1])*(p3[0] - p1[0])) < 0

def get_angle(p1, p2, p3):
    a,b,c = np.array(p1),np.array(p2),np.array(p3)
    ba = a - b
    bc = c - b

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    return np.arccos(cosine_angle)