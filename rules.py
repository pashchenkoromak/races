from common import *
size = [15, 15]
faster_color = 'b'
slower_color = 'r'

def check_kils(units):
    for i, faster in enumerate(units):
        if faster['color'] != faster_color:
            continue
        for slower in units:
            if slower['color'] != slower_color:
                continue
            if too_close(faster, slower):
                units.pop(i)

def game_over(units):
    check_kils(units)
    fast = find_units(units, need_color = faster_color)
    return len(fast) == 0
        
    
def apply_field_limit(newPos):        
    if newPos[0] < 0:
        newPos[0] = 0
    if newPos[1] < 0:
        newPos[1] = 0
    if newPos[0] > size[0]:
        newPos[0] = size[0]
    if newPos[1] > size[1]:
        newPos[1] = size[1]
    return newPos

def apply_units_collisions(units, id, newPos):
    oldUnit = units[id]
    unit = {'position': newPos, 'color':oldUnit['color'], 'size':oldUnit['size'], 'speed':oldUnit['speed']}
    move_vec = newPos - oldUnit['position']
    friends = find_units(units, need_color=unit['color'], except_id=id)
    for friend in friends:
        if too_close(unit, friend):
            possible_step = dist(oldUnit, friend) - unit['size'] - friend['size']
            k = possible_step / unit['speed']
            move_vec = move_vec * k
            newPos = oldUnit['position'] + move_vec
            unit['position'] = np.array(newPos)
    return newPos

def update_move(units, id, move_vec):
    unit = units[id]
    newPos = unit['position'] + move_vec
    newPos = apply_field_limit(newPos)
    newPos = apply_units_collisions(units, id, newPos)
    return newPos - unit['position']
    