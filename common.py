from operator import xor

from numpy.core.defchararray import equal
from geom import *

def find_units(units, need_color = None, except_color = None, except_id = -1):
    res = []
    for i, unit in enumerate(units):
        if i == except_id:
            continue
        if unit["color"] != need_color and need_color != None:
            continue
        if unit["color"] == except_color and except_color != None:
            continue
        res.append(unit)
    return res

def dist(l, r):
    return np.linalg.norm(r['position'] - l['position'])

def get_vec(l, r):
    return r['position'] - l['position']

def find_closest(units, unit):
    res = None
    min_dist = 100000
    for u in units:
        d = dist(unit, u)
        if min_dist > d:
            min_dist = d
            res = u
    return res

def too_close(l, r, size_multiplier = 1):
    return dist(l, r) <= (l['size'] + r['size']) * size_multiplier

def get_units_on_side(units, unit, vec, right = True):
    res = []
    p1 = unit['position']
    p2 = p1 + vec
    for u in units:
        if unit_eq(u, unit):
            continue
        p3 = u['position']
        side = get_side(p1, p2, p3)
        if side == right:
            res.append(u)
    return res

def unit_eq(l, r):
    for values in l:
        x_values = l[values]
        y_values = r[values]
        if isinstance(x_values, np.ndarray):
            if not np.array_equal(x_values, y_values):
                return False
        else:
            if x_values != y_values:
                return False
    return True